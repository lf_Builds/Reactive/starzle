import React, { Component } from "react";

export default class Links extends Component {
  render() {
    return (
      <ul className="nav" id="nav">
        <li id="home" className="nav-items">
          <a href="" id="home">
            Home
          </a>
        </li>
        <li id="services" className="nav-items">
          <a href="" id="services">
            Services
          </a>
        </li>
        <li id="pricing" className="nav-items">
          <a href="" id="pricing">
            Pricing
          </a>
        </li>
        <li id="booking" className="nav-items">
          <a href="" id="booking">
            Booking
          </a>
        </li>
      </ul>
    );
  }
}
