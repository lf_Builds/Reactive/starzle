import React, { Component } from "react";
import Button from "./Button";

// Lets use a stateless component for this next time

export default class Hero extends Component {
  render() {
    return (
      <div className="hero">
        <h1>Redefine Your Experience</h1>
        <h4><span>starzle</span> brings the comfort of home</h4>
        <Button name="Get Booking" id="get_booking"/>
        <Button name="Make Reservations" id="make_reservation"/>
      </div>
    );
  }
}
