import React, { Component } from 'react';

export default class Button extends Component {
    render() {
        return (
            <button className="button btn">{this.props.name}</button>
        )
    }
}