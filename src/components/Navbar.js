import React, { Component } from "react";
import Logo from "./Logo";
import Links from "./Links";

export default class Navbar extends Component {
  render() {
    return (
      <div className="navbar">
        <Logo />
        <Links />
      </div>
    );
  }
}
