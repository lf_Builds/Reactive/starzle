import React, { Component } from 'react';
import '../styles/App.css';
import Navbar from './Navbar';
import Hero from './Hero';

export default class Header extends Component {
    render() {
        return (
            <header className="header">
                <Navbar />
                <Hero />
            </header>
        )
    }
}