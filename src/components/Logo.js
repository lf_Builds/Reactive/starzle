import React, { Component } from "react";

export default class Logo extends Component {
  render() {
    return (
      <div className="logo">
            <div className="logo-brand" id="logo-brand">
                <h2>star<span>zle</span></h2>
                <pre>experience redefined</pre>
            </div>
      </div>
    );
  }
}
